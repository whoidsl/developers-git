# README #

This README would normally document whatever steps are necessary to get your application up and running.
But none of this is really required to run.  So there's that too.

### What is this repository for? ###

Everybody gets a folder with their username.  Inside that folder you can put whatever hacks, toys, junk, 
or general code cruft you want to hang onto in there.  

Don't expect anything in here to run.  But who knows what brilliance lurks in the hearts of sentroids?

### Contribution guidelines ###

* Please only put code in your directory.  Don't delete stuff.
* If these rules don't work out, email the sentry-engineering group and we'll figure it out.

### Who do I talk to? ###

* sentry-engineering
* ivaughn@whoi.edu
